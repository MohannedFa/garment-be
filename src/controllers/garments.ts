import { RequestHandler } from 'express' //eslint-disable-line
import { validationResult } from 'express-validator'
import { CreateGarmentRequest, GetGarmentsRequest, JsonError } from '../types/garments' //eslint-disable-line
import Garment from '../models/Garment'

export const createGarment: RequestHandler = async (req: CreateGarmentRequest, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const error: JsonError = new Error('Validation error! Please check your inputs.')
      error.statusCode = 400
      throw error
    }

    const { color } = req.body
    const { size } = req.body
    const { quantity } = req.body
    let batch = 1
    const garments = await Garment.find({})
    if (garments.length >= 1) batch = garments[garments.length - 1].batch + 1

    const newGarment = new Garment({
      color,
      size,
      quantity,
      batch,
    })

    await newGarment.save()
    res.status(201).json({ garment: newGarment })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

export const getGarments: RequestHandler = async (req: GetGarmentsRequest, res, next) => {
  try {
    const garments = await Garment.find({})
    res.status(200).json({ garments })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
