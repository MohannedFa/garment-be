import express, { Response, Request, NextFunction } from 'express' //eslint-disable-line
import { json } from 'body-parser'
import mongoose from 'mongoose'
import GarmentsRoutes from './routes/garments'
import { JsonError } from './types/garments' //eslint-disable-line

const app = express()

app.use(json())

app.use((req, res, next) => { //eslint-disable-line
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200)
  }
  next()
})

app.use('/garments', GarmentsRoutes)

app.use((err: JsonError, req: Request, res: Response, next: NextFunction) => { //eslint-disable-line
  const status = err.statusCode || 500
  res.status(status).json({ message: err.message })
})

mongoose.connect('mongodb+srv://mohannedm:zip123@cluster0.usvsi.mongodb.net/garments?retryWrites=true&w=majority')
  .then(() => {
    app.listen(4000)
  })
