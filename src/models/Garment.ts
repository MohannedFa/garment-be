import mongoose, { Schema } from 'mongoose'
import { GarmentDocument } from '../types/garments' //eslint-disable-line

const GarmentSchema = new Schema({
  batch: {
    type: Number,
    unique: true,
    auto: true,
    default: 1,
  },
  size: {
    type: String,
    required: true,
  },
  color: {
    required: true,
    type: String,
  },
  quantity: {
    type: Number,
    required: true,
  },
})

export default mongoose.model<GarmentDocument>('Garment', GarmentSchema)
