import { Router } from 'express'
import { check } from 'express-validator'
import * as GarmentsController from '../controllers/garments'

const router = Router()

router.post('/', [
  check('size').trim().isString().isLength({ min: 1, max: 12 }),
  check('color').trim().isString().isLength({ min: 2, max: 20 }),
  check('quantity').trim().isNumeric(),
], GarmentsController.createGarment)

router.get('/', GarmentsController.getGarments)

export default router
