import { Document } from 'mongoose' //eslint-disable-line
import { Request } from 'express' //eslint-disable-line

export interface GarmentDocument extends Document {
    batch: number
    size: string
    color: string
    quantity: number
}

export interface CreateGarmentRequest extends Request{
    body: {
        size: string
        color: string
        quantity: number
    }
}

export interface GetGarmentsRequest extends Request{}

export interface JsonError extends Error{
    statusCode?: number;
}
